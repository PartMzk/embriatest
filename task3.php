<?php

class FileIterator implements SeekableIterator
{
    private $handle;
    private $fileName;
    private $lineText = null;
    private $lineKey = 0;

    function __construct($fileName)
    {
        $this->fileName = $fileName;
        $this->handle = @fopen($fileName, 'r');

        if (!$this->handle)
        {
            throw new RuntimeException("Cannot open file $this->fileName");
        }
    }

    function rewind()
    {
        $this->freeLine();
        fseek($this->handle, 0);
        $this->lineKey = 0;
    }

    function valid()
    {
        return !$this->eof();
    }

    function current()
    {
        if (is_null($this->lineText))
        {
            $this->lineText = $this->getCurrentLine();
        }

        return $this->lineText;
    }

    function key()
    {
        return $this->lineKey;
    }

    function next()
    {
        $this->lineKey++;
    }

    function seek($linePosition)
    {
        $this->rewind();
        $this->lineKey = $linePosition;
    }
    
    function getCurrentLine()
    {
        $this->freeLine();
        if ($this->eof())
        {
            throw new Exception("End Of File");
        }

        for($linePosition = 0; $linePosition < $this->lineKey; $linePosition++)
        {
            $this->freeLine();
            $this->lineText = $this->fgets();
        }

        return $this->lineText;
    }

    function fgets()
    {
        $buffer = fgets($this->handle);

        return $buffer;
    }

    private function freeLine()
    {
        if ($this->lineText) {
            $this->lineText = null;
        }
    }

    function eof()
    {
        return feof($this->handle);
    }
}

$reader = new FileIterator(__FILE__);
