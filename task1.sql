/* добавление категории */
INSERT INTO categories (name) VALUES (:category_name);

/* обновиление категории */
UPDATE categories SET name = 'name' WHERE id = :category_id;

/*Удаление категории*/
BEGIN;
DELETE FROM posts WHERE category_id = :category_id;
DELETE FROM categories WHERE id = :category_id;
COMMIT;

/* добавление поста */
INSERT INTO posts (category_id, content, user_id) VALUES (:category_id, :content, :user_id);
/* или */
INSERT INTO posts (content, user_id) VALUES (:content, :user_id);


/* выборка постов */
SELECT * FROM posts ORDER BY id DESC LIMIT 50;

/* выборка постов по категории */
SELECT * FROM posts WHERE category_id = :category_id ORDER BY id DESC LIMIT 50;
/* или */
SELECT * FROM posts WHERE category_id IS NULL ORDER BY id DESC LIMIT 50;

/* like посту */
INSERT INTO likes (post_id, user_id) VALUES (:post_id, :user_id);

/* dislike посту */
DELETE FROM likes WHERE post_id = :user_id AND user_id = :user_id;

/* выборка плоьзователей, оценивших пост */
SELECT user_id FROM likes WHERE post_id = :post_id;
