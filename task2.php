<?php
$start = microtime(true);

$user = 'root';
$pass = 'pass';
$dataBaseName = 'task2';

$userTableName = 'users';
$tableWithEmails = 'user_emails';

$dbh = createDataBaseConnection($user, $pass, $dataBaseName);

if (!tableIsExist($dbh, $userTableName)) {
    exit('Table `users` is not exist.');
}

if (tableIsExist($dbh, $tableWithEmails)) {
    dropTable($dbh, $tableWithEmails);
}

createTableWithEmailsFromTable($dbh, $tableWithEmails, $userTableName);

$limitByStep = 100000;
$emailDomains = [];
$delimiter = ',';
while (true) {
    $domains = [];
    $startPosition = ($startPosition === null) ? 0 : ($startPosition += $limitByStep);

    $selectEmailsQuery = "SELECT emails FROM " . $tableWithEmails . " LIMIT " . $startPosition . ", " . $limitByStep . ";";
    $selectExec = $dbh->query($selectEmailsQuery);

    while ($emails = $selectExec->fetchColumn(0)) {

        if (strpos($emails, $delimiter) !== false) {
            $emails = explode($delimiter, $emails);

            foreach ($emails as $email) {
                $domains[] = getEmailDomain($email);
            }
        } else {
            $domains[] = getEmailDomain($emails);
        }
    }

    if (!empty($domains)) {
        $getCountDomains = getEmailCountByDomains($domains);

        foreach ($getCountDomains as $domainName => $count) {
            $emailDomains[$domainName][] = $count;
        }

        usleep(0.5 * 1000000); //0.5sec
    }

    if ($selectExec->rowCount() < $limitByStep) {
        break;
    }
}
$dbh = null;

echo '<pre>' . print_r(calculateEmailDomains($emailDomains), 1) . '</pre>' . __LINE__ . ' # ' . __FILE__ . '<br/>' . "\n";

echo 'Duration: ' . (microtime(true) - $start) . 's';

function calculateEmailDomains(array $domains)
{
    $calculatedEmailDomains = [];

    foreach ($domains as $domain => $counts) {
        $calculatedEmailDomains[$domain] = array_sum($counts);
    }

    return $calculatedEmailDomains;
}

function getEmailCountByDomains(array $domains)
{
    $emailCountByDomains = array_count_values($domains);

    return $emailCountByDomains;
}

function getEmailDomain($string)
{
    $emailDomain = 'undefined';
    $position = strpos($string, '@');

    if ($position !== false) {
        $emailDomain = substr($string, ($position + 1));
    }

    return $emailDomain;
}

function createDataBaseConnection($user, $pass, $dataBaseName, $host = "localhost")
{
    try {
        $dbh = new PDO('mysql:host=' . $host . ';dbname=' . $dataBaseName, $user, $pass);

        return $dbh;
    } catch (PDOException $e) {
        die($e->errorInfo);
    }
}

function createTableWithEmailsFromTable(PDO $dbh, $tableWithEmails, $userEmailsTableName)
{
    try {
        $createUserEmailsTableQuery = "CREATE TABLE " . $tableWithEmails . " ENGINE=MyISAM as (SELECT REPLACE(email, ' ', '') as emails FROM " . $userEmailsTableName . " WHERE email <> '');";
        $dbh->query($createUserEmailsTableQuery);
    } catch (PDOException $e) {
        die($e->errorInfo);
    }
}

function tableIsExist(PDO $dbh, $tableName)
{
    $res = $dbh->query("SHOW TABLES LIKE '" . $tableName . "';");

    if ($res->rowCount() !== 0) {
        return true;
    }

    return false;
}

function dropTable(PDO $dbh, $tableName)
{
    try {
        $dbh->query("DROP TABLE " . $tableName . ";");
    } catch (PDOException $e) {
        die($e->errorInfo);
    }
}
